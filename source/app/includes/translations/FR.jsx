var TRANS = {

    INIT: {
        CANCEL: "Le script a été annulé",
    },

    VIEW: {
        TITLE: "Import layers / version %s",
        LAYERS: "Calques à importer",
        ITEMS: "Éléments à importer",
        TARGETS: "Targets",
        BTN_TARGET: "Ajouter un document",
        BTN_TARGET_PLACEHOLDER: "Ajouter un document cible",
        RUN: "Go"
    },

    CONTROLLER: {
        FINISHED: "Le script est terminé",
        EMPTY_DOC: "Le formulaire n'est pas valide. Veuillez ajouter un document cible",
    }

}