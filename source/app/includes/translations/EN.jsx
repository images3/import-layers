var TRANS = {

    INIT: {
        CANCEL: "The Script has been forced to cancel",
    },

    VIEW: {
        TITLE: "ImportLayers / version %s",
        LAYERS: "Layers to import",
        ITEMS: "Items to import",
        TARGETS: "Targets",
        BTN_TARGET: "add document",
        BTN_TARGET_PLACEHOLDER: "Please select the target document",
        RUN: "Go"
    },

    CONTROLLER: {
        FINISHED: "script finished",
        EMPTY_DOC: "The form is not valid. Please add a target document",
    }

}