/**
 * Function to generate the controller
 * @param {Object} view the view
 * @return {Object} the controller
 */
var mainController = function (view, doc_source) {

    /***********************************************
     * Properties
     ***********************************************/

    var that = {};

    // privates
    var my = {};

    my.array_of_checkbox = [];
    my.mainView = view;
    my.doc_source = doc_source;

    /***********************************************
     * Functions
     ***********************************************/


    /**
     * Function main
     */
    that.run = function () {

        var target_doc;

        is_form_valid();

        target_doc = IN.Document.open(my.mainView.target_path.text);

        // make sure that the document is valid
        is_document_valid(my.doc_source, target_doc);

        // duplicate very layers
        import_layers(my.doc_source, target_doc);

    }

    /**
     * Function to valid the form
     * @param {View} mainView the view
     * @param {Folder} folder_source the source folder
     */
    function is_form_valid() {

        var my_file = new File(my.mainView.target_path.text);

        if (!my_file.exists) {

            throw {
                name: 'InvalidArgumentError',
                message: TRANS.CONTROLLER.EMPTY_DOC.printf(),
                fileName: $.fileName,
                lineNumber: $.line
            };

        }

    }

    /**
     * Function to import a layer from a source document to an other
     * @param source_doc
     * @param target_doc
     */
    function import_layers(source_doc, target_doc) {


        var source_layers = source_doc.layers;
        var count = source_layers.length;
        var tf_map = {};
        var link_map = [];

        var current_layer;
        var all_items;
        var count_frames;
        var current_layer_name;
        var checkbox_layer;

        // loop on this array from the end to respect layer order
        for (var i = count - 1; i >= 0; i--) {

            current_layer_name = source_layers[i].name;
            checkbox_layer = my.array_of_checkbox.find('text', current_layer_name);

            // add only layer that the user want to add
            if (checkbox_layer.value && checkbox_layer.text === current_layer_name) {

                current_layer = target_doc.layers.add(
                    {name: current_layer_name}
                );

                all_items = source_layers[i].pageItems.everyItem().getElements();
                count_frames = all_items.length;

                for (var j = count_frames - 1; j >= 0; j--) {

                    duplicate_item(all_items[j], current_layer, tf_map, link_map);

                }

            }

        }

        // if the user copied text frames > repair links
        if (my.mainView.items_textframes.value) {

            repair_text_frames_links(link_map, tf_map);

        }

    }

    /**
     * Function to copy a page item
     * @param my_item
     * @param current_layer
     * @param tf_map
     * @param link_map
     */
    function duplicate_item(my_item, current_layer, tf_map, link_map) {

        // copy the text frame only if the user choosed that option
        if (my_item.constructor.name === 'TextFrame') {

            if (my.mainView.items_textframes.value) {

                duplicate_text_frame(my_item, current_layer, tf_map, link_map);

            }

            return;

        }

        // if the page item is a rectangle check property images > if not equal to 0 this is a graphic
        if (my_item.hasOwnProperty('images') && my_item.images.length > 0) {

            if (my.mainView.items_graphics.value) {

                my_item.duplicate(current_layer);

            }

            return

        }

        // if this is not a text frame or a graphic > this is an other object
        if (my.mainView.items_others.value) {

            my_item.duplicate(current_layer);

        }

    }


    /**
     * Function to copy an item to an other layer
     * keep each text frame id to rebuild text threading
     * @param item
     * @param dest_layer
     * @param tf_map
     * @param link_map
     */
    function duplicate_text_frame(item, dest_layer, tf_map, link_map) {

        // duplicate the text frame
        tf_map[item.id] = item.duplicate(dest_layer);

        if (typeof item.previousTextFrame !== "undefined" && item.previousTextFrame && item.textFrameIndex > 0) {

            link_map.push({
                item: item,
                prev: item.previousTextFrame
            });

        }

    };


    /**
     * Function rebuild text frame threading after a copy / paste
     * @param link_map
     * @param tf_map
     */
    function repair_text_frames_links(link_map, tf_map) {

        var old_item;
        var parent_item;
        var new_prev_item;

        for (var i = 0; i < link_map.length; i++) {

            old_item = link_map[i].item;
            parent_item = tf_map[old_item.id];

            if (link_map[i].prev) {

                new_prev_item = tf_map[link_map[i].prev.id];
                parent_item.previousTextFrame = new_prev_item; // Set previous (and implicitly merge contents)

            }

        }

    };


    /**
     * Function to check if a document is valid
     * (same number of pages in source and target document, same size, same number of spread)
     * @param source_doc
     * @param target_doc
     */
    function is_document_valid(source_doc, target_doc) {

        if (source_doc.pages.length !== target_doc.pages.length) {

            throw {
                name: 'InvalidDocumentError',
                message: 'number of pages of both documents must be the same',
                fileName: $.fileName,
                lineNumber: $.line
            };

        }

        if (source_doc.documentPreferences.pageWidth !== target_doc.documentPreferences.pageWidth) {

            throw {
                name: 'InvalidDocumentError',
                message: 'the witdh of both document aren\'t the same',
                fileName: $.fileName,
                lineNumber: $.line
            };

        }

        if (source_doc.documentPreferences.pageHeight !== target_doc.documentPreferences.pageHeight) {

            throw {
                name: 'InvalidDocumentError',
                message: 'height of both document aren\'t the same',
                fileName: $.fileName,
                lineNumber: $.line
            };

        }

        for (var i = 0; i < source_doc.spreads.length; i++) {

            if (source_doc.spreads[i].pages.length !== target_doc.spreads[i].pages.length) {

                throw {
                    name: 'InvalidDocumentError',
                    message: 'Documents do not have the same number of spreads per page',
                    fileName: $.fileName,
                    lineNumber: $.line
                };

            }

        }

    }


    /**
     *
     */
    function init() {


        var layers = my.doc_source.layers;

        // add layer to group
        if (layers) {

            /**
             * build a matrices with x rows of 3 element
             * each row is a new group
             */
            var MAX_ELEMENT_ROW = 3;
            var NUMBER_OF_ROW = Math.ceil(layers.length / MAX_ELEMENT_ROW);
            var i = 0;

            for (var row = 0; row < NUMBER_OF_ROW; row++) {

                // build a new row
                var grp = my.mainView.pans.layers.group('layers', {
                    orientation: 'row',
                    alignChildren: 'left',
                    alignment: 'top'
                });

                /**
                 * add maximum 3 checkbox per row
                 * if the index is superior than array.lenght stop
                 */
                for (var el = 0; el < MAX_ELEMENT_ROW; el++) {

                    if (i < layers.length) {

                        var my_checkbox = grp.add('checkbox', layers[i].name);
                        my.array_of_checkbox.push(my_checkbox);
                        i++;

                    }
                    else {
                        break;
                    }
                }

            }
        }


    }


    /**
     * Function to update the target folder
     * @param {Folder} target
     */
    function update_target(target) {

        my.mainView.target_path.text = target;

    }

    /***********************************************
     * Events handler
     ***********************************************/


    /**
     * Function to set the target folder path
     */
    function set_target() {

        var files = File.openDialog(
            'Select a document',
            H.Utils.get_file_filter(['.indd'], 'Select a document'), true
        );


        // if the user do not cancel the add dialog box update the source
        if (files) {
            update_target(new File(files).fsName);
        }


    }

    // add events listeners
    my.mainView.target_add.addEventListener('click', set_target);

    init();

    return that;

};

